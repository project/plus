<?php

namespace Drupal\plus_enhancements\Plugin\Enhancement;

/**
 * Defines the interface for an enhancement plugin.
 *
 * @ingroup plugins_enhancements
 */
interface EnhancementInterface {

}
