<?php

namespace Drupal\plus\Annotation;

use Drupal\Component\Annotation\PluginID;

/**
 * Defines an Theme annotation object.
 *
 * Plugin Namespace: "Plugin/Theme".
 *
 * @see \Drupal\plus\ThemeInterface
 * @see \Drupal\plus\ThemeManager
 * @see plugin_api
 *
 * @Annotation
 *
 * @ingroup plugins_theme
 */
class Theme extends PluginID {}
