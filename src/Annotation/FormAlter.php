<?php

namespace Drupal\plus\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a FormAlter annotation object.
 *
 * Plugin Namespace: "Plugin/FormAlter".
 *
 * @see \Drupal\plus\Core\Form\FormAlterInterface
 * @see \Drupal\plus\FormAlterPluginManager
 * @see plugin_api
 *
 * @Annotation
 *
 * @ingroup plugins_form
 */
class FormAlter extends Plugin {}
