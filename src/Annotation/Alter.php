<?php

namespace Drupal\plus\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an Alter annotation object.
 *
 * Plugin Namespace: "Plugin/Alter".
 *
 * @see \Drupal\plus\AlterInterface
 * @see \Drupal\plus\AlterManager
 * @see plugin_api
 *
 * @Annotation
 *
 * @ingroup plugins_alter
 */
class Alter extends Plugin {}
