<?php

namespace Drupal\Tests\plus\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * {@inheritdoc}
 */
abstract class PlusKernelTestBase extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['plus'];

}
