/**
* DO NOT EDIT THIS FILE.
* THIS FILE IS COMPILED AUTOMATICALLY FROM ITS ES6 SOURCE.
* @preserve
**/'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

(function (Drupal) {
  'use strict';

  var _ = {
    assetType: new WeakMap()
  };

  var Asset = function (_Drupal$Url) {
    _inherits(Asset, _Drupal$Url);

    function Asset() {
      _classCallCheck(this, Asset);

      var _this = _possibleConstructorReturn(this, (Asset.__proto__ || Object.getPrototypeOf(Asset)).call(this));

      _.assetType.set(_this, null);
      return _this;
    }

    _createClass(Asset, [{
      key: 'assetType',
      get: function get() {
        var _this2 = this;

        return this.getPrivateProperty(_, 'assetType', function () {
          var hash = _this2.hash;
          var query = _this2.query;
          var url = _this2.absolute.replace(/#.*$/, '').replace(/\?.*$/, '');
          var assetType = 'js';

          if (/^@[\w]+/.test(url)) {
            assetType = 'library';
            url = url.replace(/^@/, '');
          } else {
            var match = url.match(/^(css|js):\/\/|\.(css|js)$/);
            if (match) {
              assetType = match[1] || match[2] || 'js';
            }
            url = url.replace(/^(css|js):\/\//, '');
          }

          if (_this2.absolute !== url + query + hash) {
            _this2.absolute = url + query + hash;
          }

          return assetType;
        });
      },
      set: function set(value) {
        Drupal.error('The type of an asset is determined automatically from the absolute URL and cannot be set manually.');
      }
    }]);

    return Asset;
  }(Drupal.Url);

  Drupal.Asset = Asset;
})(window.Drupal);